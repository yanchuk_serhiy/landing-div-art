
$("#partners").owlCarousel({
    loop:true,
    margin:15,
    dots: true,
    nav: false,
    mouseDrag: false,
    autoplay: true,
    autoplayTimeout: 5000,
    // navContainer:true,
    // dotsContainer: true,
    responsive:{
        0:{
            items:1
        },
        400:{
            items:1
        },
        500:{
            items:1
        },
        700:{
            items:2
        },
        1023:{
            items:3,
            mouseDrag: false
        },
        1200:{
            items:4,
            mouseDrag: true
        }

    }
});

$("#owl-howWorks").owlCarousel({
    margin:0,
    dots: true,
    nav: false,
    mouseDrag: false,
    autoplay: true,
    autoplayTimeout: 5000,
    loop: true,
    responsive:{
        0:{
            items:1
        },
        400:{
            items:1
        },
        500:{
            items:1
        },
        700:{
            items:1
        },
        1023:{
            items:1,
            mouseDrag: false
        },
        1200:{
            items:1,
            mouseDrag: true
        }

    }
});

var owl = $('#owl-howWorks');
owl.owlCarousel();
// Случаешь события карусели:
owl.on('changed.owl.carousel', function(event) {
    var item = event.item.index;     // Позиция текущего слайда
    // console.log(item);
    if (item === 2) {
        $('.step-2').removeClass('step-active');
        $('.step-3').removeClass('step-active');
        $('.step-4').removeClass('step-active');
    }
    if (item === 3) {
        $('.step-2').addClass('step-active');
        $('.step-3').removeClass('step-active');
        $('.step-4').removeClass('step-active');
    }
    if (item === 4) {
        $('.step-2').addClass('step-active');
        $('.step-3').addClass('step-active');
        $('.step-4').removeClass('step-active');
    }
    if (item === 5) {
        $('.step-2').addClass('step-active');
        $('.step-3').addClass('step-active');
        $('.step-4').addClass('step-active');
    }
});



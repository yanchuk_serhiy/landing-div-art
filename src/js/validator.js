function validate(form) {
    var form = $(form);
    var inputs = form.find('input');
    var valid = true;

    if (inputs.length > 0){
        for (var i = 0; i < inputs.length; i++){
            var validTMP = validateInput($(inputs[i]));

            if(valid){
                valid = validTMP;
            }
        }
    }

    return valid;
}

function validateInput(el) {
    var valid = true;
    if (el.val() === ''){
        el.addClass('incorect');
        el.attr('placeholder', 'required...');
        valid = false;
    }else{
        var emailCorrect = true;
        if(el.attr('name') === 'email'){
            emailCorrect = validateEmail(el.val());
        }
        if (emailCorrect){
            el.removeClass('incorect');
            el.attr('placeholder', el.attr('data-placeholder'));
            valid = true;
        }else{
            el.val('');
            el.addClass('incorect');
            el.attr('placeholder', 'incorrect email');
            valid = false;
        }
    }
    
    return valid;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

